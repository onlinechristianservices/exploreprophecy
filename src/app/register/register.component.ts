import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Overlay, OverlayConfig } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { ProgressOverlayComponent } from '../progress-overlay/progress-overlay.component';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;

  constructor(private fb: FormBuilder,
    private client: HttpClient,
    private router: Router,
    private overlay: Overlay) {

    this.form = fb.group({
      'name': ['', Validators.required],
      'address': ['', Validators.required],
      'city': ['', Validators.required],
      'state': ['', Validators.required],
      'zip': ['', Validators.required],
      'email': ['', Validators.email],
      'notifyViaEmail': false,
      'phone': ['', Validators.pattern('\\d{3}[- ]?\\d{3}-?\\d{4}$')],
      'notifyViaText': false,
      'numberAttending': [1, Validators.min(1)],
      'numberOfChildrenAttending': '',
    });
  }

  ngOnInit() {
  }

  register() {

    let config = new OverlayConfig();
    config.positionStrategy = this.overlay.position()
      .global()
      .centerHorizontally()
      .centerVertically();

    config.hasBackdrop = true;
    let ref = this.overlay.create(config);
    ref.attach(new ComponentPortal(ProgressOverlayComponent));

    if (!this.form.value.numberAttending) {
      this.form.value.numberAttending = 1;
    }
    if (!this.form.value.numberOfChildrenAttending) {
      this.form.value.numberOfChildrenAttending = 0;
    }

    this.client.post('api/r', this.form.value)
      .subscribe(_ => {
        ref.dispose();
        this.router.navigate(['/thankyou']);
      });
  }

}
