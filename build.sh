ver=$1

if [[ ! -n "$1" ]]; then

	version=$(grep :v[[:digit:]] k8s.yaml | awk -F ':v' '{print $2}')
	((ver=version+1))
	echo "attempt to set version to $ver"
fi

	rm -fdr publish
	dotnet publish -o publish -c Release
	docker build -t ocsregistry.azurecr.io/exploreprophecy:v$ver .
	az acr login -n ocsregistry
	docker push ocsregistry.azurecr.io/exploreprophecy:v$ver

	sed -i '' "s/:v[[:digit:]]*/:v$ver/" ./k8s.yaml

	echo "k8s.yaml has been updated for image version $1. 'kubectl apply' at your convenience'
